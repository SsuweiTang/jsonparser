package com.example.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	private Button bt_json = null;
	TextView tv_json = null;
	String str_json = "";
	String res = null;
	private List<Channel> listjson = null;
	Thread threadjson = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		tv_json = (TextView) findViewById(R.id.textView1);
		bt_json = (Button) findViewById(R.id.button1);
		bt_json.setText("json parse from file");
		tv_json.setText("");
		gettxt();

	}

	private void gettxt() {
		// TODO Auto-generated method stub
		String fileName = "json.txt";

		InputStream in;
		try {
			in = getResources().getAssets().open(fileName);
			int length;
			length = in.available();
			byte[] buffer = new byte[length];
			in.read(buffer);
			res = EncodingUtils.getString(buffer, "UTF-8");
			//
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		JSONObject jsonRootObject;
		try {
			jsonRootObject = new JSONObject(res);
			JSONArray jsonArray = jsonRootObject.optJSONArray("channel");

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject;

				jsonObject = jsonArray.getJSONObject(i);

				String id = jsonObject.optString("id").toString();
				String name = jsonObject.optString("name").toString();

				String imgurl = jsonObject.optString("channel_img").toString();

				String einame = "", start = "", end = "";
				JSONArray jsonArray2 = jsonObject.optJSONArray("liveBroadcast");
				for (int j = 0; j < jsonArray2.length(); j++) {
					JSONObject jsonObject2 = jsonArray2.getJSONObject(j);
					einame = jsonObject2.optString("name").toString();
					start = jsonObject2.optString("startTime").toString();
					end = jsonObject2.optString("endTime").toString();
				}

				str_json += "Node" + i + " : \n id= " + id + " \n Name= "
						+ name + "\n " + "einame" + einame + "\n "
						+ "starttime" + start + "\n " + "endtime" + end + "\n "
						+ "channel_img" + imgurl + "\n ";
			}

		}

		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		bt_json.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String url = "http://192.168.0.103:8080/json.php";
				str_json ="";
				
				try {
					JSONArray data = new JSONArray(getJSONUrl(url));
					
					
					for(int i = 0; i < data.length(); i++){
		                JSONObject c = data.getJSONObject(i);
		                
		    		
		    			
		    			
							str_json += "Node" + i + " : \n id= " + c.getString("id") + " \n Name= "
									+c.getString("name")+ "\n " + "einame" + c.getString("einame") + "\n "
									;
						

							tv_json.setText(str_json);
				
				
					}
				}catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				

			}

		});

	}

	private String getJSONUrl(String url) {
		StringBuilder str = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) { // Download OK
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					str.append(line);
				}
			} else {
				Log.e("Log", "Failed to download result..");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str.toString();
	}
}
