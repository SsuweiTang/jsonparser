package com.example.json;

import java.io.IOException;


import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;





import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;

public class JsonParser {
	public JsonParser() {
	}


	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	public List<Channel> getChannels(String input) {
		List<Channel> list = new ArrayList<Channel>();

	try {
		JsonReader reader = new JsonReader(new StringReader(input));
		reader.beginArray(); 
			while (reader.hasNext()) {
				Channel channel = new Channel();
				reader.beginObject();

				while (reader.hasNext()) {
					String tagName = reader.nextName();
					if (tagName.equals("id")) {
						channel.setName(reader.nextString());

					} else if (tagName.equals("name")) {

						System.out.println("name"+reader.nextString());
						channel.setName(reader.nextString());

					}
					reader.endObject(); 
				}
				list.add(channel);
				channel = null;
			}
			 reader.endArray(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return list;

	}

}
